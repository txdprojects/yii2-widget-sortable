<?php

namespace txd\widgets\sortable;

use yii\web\AssetBundle;

class SortableAsset extends AssetBundle
{
	/**
	 * @inheritdoc
	 */
	public $css = [
		'css/yii.sortable.css',
	];

	/**
	 * @inheritdoc
	 */
	public $js = [
		'js/jquery.ui.touch-punch.min.js',
		'js/jquery.mjs.nestedSortable.js',
		'js/yii.sortable.js',
	];

	/**
	 * @inheritdoc
	 */
	public $depends = [
		'yii\web\JqueryAsset',
		'yii\jui\JuiAsset',
	];

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		$this->sourcePath = __DIR__ . '/assets';
	}
}
