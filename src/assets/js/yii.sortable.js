(function ($, window, document, undefined) {
	/**
	 * Constants
	 * @constant {String} PLUGIN_NAME
	 * @constant {String} PLUGIN_VERSION
	 * @constant {String} DATA_KEY
	 * @constant {Object} DEFAULTS
	 */
	var PLUGIN_NAME = 'yiiSortable',
		PLUGIN_VERSION = '1.0.0',
		EVENT_NS = '.' + PLUGIN_NAME,
		DATA_KEY = 'plugin_' + PLUGIN_NAME,
		DEFAULTS = {
			items: '.sortable-item',
			handle: '.sortable-item__handle',
			placeholder: 'sortable-item_placeholder',
			forcePlaceholderSize: true,
			helper: 'clone',
			tolerance:'pointer',
			isTree: false,
			maxLevels: 0,
			errorClass: 'sortable-item--error',
			saveOrder: null,

			onInit: function () {},
			onDestroy: function () {}
		};

	/**
	 * Plugin
	 *
	 * @param element
	 * @param options
	 * @param metadata
	 * @constructor
	 */
	var Plugin = function (element, options, metadata) {
		if (!element) {
			console.error('[' + PLUGIN_NAME + ']: DOM element is missing');
			return;
		}
		this.element = element;
		this.options = $.extend({}, DEFAULTS, options, metadata);
		this.init();
	};

	/**
	 * Initialization
	 */
	Plugin.prototype.init = function () {
		this._cacheElements();
		this.$sortable.nestedSortable(this.options);
		this._bindEvents();
		this._hook('onInit');
	};

	/**
	 * Caches DOM Elements.
	 *
	 * @private
	 */
	Plugin.prototype._cacheElements = function () {
		this.$window = $(window);
		this.$document = $(document);
		this.$body = $(document.body);
		this.$element = $(this.element);
		this.$sortable = this.$element.children('.sortable');
	};

	/**
	 * Binds Events.
	 *
	 * @private
	 */
	Plugin.prototype._bindEvents = function () {
		this.$element.on('sortupdate' + EVENT_NS, this._onSortableUpdate.bind(this));
	};

	/**
	 * Handles the jQuery UI sortable update event.
	 *
	 * @param e
	 * @param ui
	 * @private
	 */
	Plugin.prototype._onSortableUpdate = function (e, ui) {
		if (this.options.saveOrder) {
			this.saveOrder();
		}
	};

	/**
	 * Gets the order.
	 *
	 * @param asJson
	 * @private
	 */
	Plugin.prototype.getOrder = function (asJson) {
		var me = this,
			order = {};

		this.$element.find(this.options.items).each(function (index, item) {
			var $item = $(item),
				$parentItem = $item.parent().closest(me.options.items);

			order[index] = {
				id: $item.attr('data-id'),
				parentId: $parentItem.attr('data-id'),
			};
		});

		return asJson === true ? JSON.stringify(order) : order;
	};

	/**
	 * Saves the order to a specific target or through AJAX.
	 *
	 * @return {*}
	 */
	Plugin.prototype.saveOrder = function () {
		if (!this.options.saveOrder) {
			return false;
		}

		if (typeof this.options.saveOrder === 'string') {
			var $target = this.$body.find(this.options.saveOrder);

			if ($target.is(':input')) {
				$target.val(this.getOrder(true));
			} else {
				$target.text(this.getOrder(true));
			}

			return true;
		} else if (typeof this.options.saveOrder === 'object') {
			if (this._saveOrderXhr) {
				this._saveOrderXhr.abort();
			}
			this._saveOrderXhr = $.ajax($.extend({
				method: 'POST',
				data: {
					sortable: this.getOrder()
				}
			}, this.options.saveOrder));

			return this._saveOrderXhr;
		}
	};

	/**
	 * Hooks callbacks and custom events.
	 *
	 * @access private
	 * @param [arguments]
	 */
	Plugin.prototype._hook = function () {
		var args = Array.prototype.slice.call(arguments),
			hookName = args.shift(),
			eventName = '';

		if (hookName.substr(0, 2) === 'on') {
			eventName = hookName.slice(2).charAt(0).toLowerCase() + hookName.slice(3);
		} else {
			eventName = hookName.charAt(0).toLowerCase() + hookName.slice(1);
		}
		eventName += EVENT_NS;

		// Execute the callback
		if (typeof this.options[hookName] === 'function') {
			this.options[hookName].apply(this.element, args);
		}

		// Create a new event
		var event = $.Event(eventName, {
			target: this.element
		});
		// Trigger the event
		this.$element.trigger(event, args);
	};

	/**
	 * Gets or sets a property.
	 *
	 * @access public
	 * @param {String} key
	 * @param {String} val
	 */
	Plugin.prototype.option = function (key, val) {
		if (val) {
			this.options[key] = val;
		} else {
			return this.options[key];
		}
	};

	/**
	 * Destroys the plugin instance.
	 *
	 * @public
	 */
	Plugin.prototype.destroy = function () {
		this._hook('onDestroy');
		this.$window.off(EVENT_NS);
		this.$document.off(EVENT_NS);
		this.$element.off(EVENT_NS);
		this.$element.removeData(DATA_KEY);
	};

	/**
	 * Plugin definition
	 * @function external "jQuery.fn".yiiSortable
	 */
	$.fn[PLUGIN_NAME] = function (options) {
		var args = arguments;

		if (!options || typeof options === 'object') {
			return this.each(function () {
				if (!$.data(this, DATA_KEY)) {
					var metadata = $(this).data();
					$.data(this, DATA_KEY, new Plugin(this, options, metadata));
				}
			});
		} else if (typeof args[0] === 'string') {
			var methodName = args[0].replace('_', ''),
				returnVal;

			this.each(function () {
				var instance = $.data(this, DATA_KEY);

				if (instance && typeof instance[methodName] === 'function') {
					returnVal = instance[methodName].apply(instance, Array.prototype.slice.call(args, 1));
				} else {
					throw new Error('Could not call method "' + methodName + '" on jQuery.fn.' + PLUGIN_NAME);
				}
			});

			return (typeof returnVal !== 'undefined') ? returnVal : this;
		}
	};

	/**
	 * Expose global
	 */
	this[PLUGIN_NAME] = Plugin;

})(jQuery, window, document);
