<?php

namespace txd\widgets\sortable;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\web\View;

/**
 * Class Sortable
 *
 * @link https://github.com/ilikenwf/nestedSortable
 *
 * @author Tuxido <hello@tuxido.ro>
 */
class Sortable extends \yii\base\Widget
{
	/**
	 * @var array The sortable items.
	 */
	public $items = [];

	/**
	 * @var bool|string The item handler for which the drag operation works.
	 */
	public $itemHandle = true;

	/**
	 * @var array The container options.
	 */
	public $containerOptions = [];

	/**
	 * @var array The sortable options.
	 */
	public $options = [];

	/**
	 * @var array The sortable item options.
	 */
	public $itemOptions = [];

	/**
	 * @var array The message to be displayed if the items are empty.
	 */
	public $emptyOptions = [];

	/**
	 * @var string HTML tag element for the sortable item parent.
	 */
	protected $tag = 'ol';

	/**
	 * @var string HTML tag element for the sortable item.
	 */
	protected $itemTag = 'li';

	/**
	 * @var array The client (JS) options.
	 */
	public $clientOptions = [];

	/**
	 * @var array The client (JS) events.
	 */
	public $clientEvents = [];

	/**
	 * @var string The client (JS) selector.
	 */
	private $_clientSelector;

	/**
	 * @var string The global widget JS hash variable.
	 */
	private $_hashVar;

	/**
	 * @inheritdoc
	 * @throws \yii\base\InvalidConfigException
	 */
	public function init()
	{
		parent::init();

		if (!is_array($this->items)) {
			throw new InvalidConfigException('The "items" property must be an array.');
		}

		$this->setupProperties();
		$this->registerAssets();

		ob_start();
	}

	/**
	 * @inheritdoc
	 * @throws InvalidConfigException
	 */
	public function run()
	{
		$content = ob_get_clean();

		if (!empty($this->items)) {
			$content = Html::tag($this->tag, $this->renderItems($this->items), $this->options);
		} else {
			if (empty($content)) {
				$message = ArrayHelper::remove($this->emptyOptions, 'message');
				$content = Html::tag($this->itemTag, $message, $this->emptyOptions);
			}
			$content = Html::tag($this->tag, $content, $this->options);
		}

		return Html::tag('div', $content, $this->containerOptions);
	}

	/**
	 * Renders the items.
	 *
	 * @param array $items
	 * @return string
	 * @throws InvalidConfigException
	 */
	protected function renderItems($items)
	{
		$lines = [];

		if (empty($items)) {
			return '';
		}

		foreach ($items as $item) {
			$content = $item;
			$itemOptions = $this->itemOptions;
			$itemCallback = ArrayHelper::remove($itemOptions, 'item', null);

			if (is_array($item)) {
				if (!isset($item['content'])) {
					$item['content'] = '***MISSING_CONTENT***';
				}
				$itemOptions = array_merge($itemOptions, ArrayHelper::getValue($item, 'options', []));
				Html::addCssClass($itemOptions, 'sortable-item');
				if (isset($item['id'])) {
					$itemOptions['data-id'] = $item['id'];
				}
				$content = $item['content'];
			}
			if ($itemCallback instanceof \Closure) {
				$content = call_user_func_array($itemCallback, [&$item, $content]);
				if (isset($item['options'])) {
					$itemOptions = array_merge_recursive($itemOptions, $item['options']);
				}
			}
			if ($this->itemHandle !== false) {
				$content = $this->itemHandle . $content;
			}
			$content = Html::tag('div', $content, ['class' => 'sortable-item__content']);

			// Add the nested list
			if (is_array($item) && is_array($item['items'])) {
				$content .= Html::tag($this->tag, $this->renderItems($item['items']));
			}

			$lines[] = Html::tag($this->itemTag, $content, $itemOptions);
		}

		return implode("\n", $lines);
	}

	/**
	 * Gets the client selector.
	 *
	 * @return string
	 */
	public function getClientSelector()
	{
		if (!$this->_clientSelector) {
			$this->_clientSelector = '#' . $this->getId();
		}
		return $this->_clientSelector;
	}

	/**
	 * Gets the hash variable.
	 *
	 * @return string
	 */
	public function getHashVar()
	{
		if (!$this->_hashVar) {
			$this->_hashVar = 'sortable_' . hash('crc32', $this->buildClientOptions());
		}
		return $this->_hashVar;
	}

	/**
	 * Sets the widget properties.
	 */
	protected function setupProperties()
	{
		$this->containerOptions = array_merge([
			'id' => $this->getId(),
			'class' => 'sortable-container',
			'data' => [
				'sortable-options' => $this->getHashVar(),
			],
		], $this->containerOptions);
		Html::addCssClass($this->containerOptions, 'sortable-container');
		$this->containerOptions['data']['sortable-options'] = $this->getHashVar();
		if ($this->containerOptions['id']) {
			$this->setId($this->containerOptions['id']);
		}

		Html::addCssClass($this->options, 'sortable');
		Html::addCssClass($this->itemOptions, 'sortable-item');

		$this->tag = ArrayHelper::remove($this->options, 'tag', 'ol');
		$this->itemTag = ArrayHelper::remove($this->itemOptions, 'tag', 'li');

		if ($this->itemHandle === true) {
			$this->itemHandle = Html::tag('span', null, [
				'class' => 'sortable-item__handle fa fa-arrows-alt',
			]);
		}
		if ($this->itemHandle !== false) {
			Html::addCssClass($this->itemOptions, 'sortable-item--with-handle');
		}
	}

	/**
	 * Builds the client options.
	 *
	 * @return string
	 */
	protected function buildClientOptions()
	{
		$this->clientOptions = array_merge([

		], $this->clientOptions);

		if ($this->itemHandle && !$this->clientOptions['handle']) {
			$this->clientOptions['handle'] = '.sortable-item__handle';
		}
		if (!$this->clientOptions['listType']) {
			$this->clientOptions['listType'] = $this->tag;
		}

		return Json::encode($this->clientOptions);
	}

	/**
	 * Registers the widget assets.
	 */
	protected function registerAssets()
	{
		$view = $this->getView();

		// Register assets
		SortableAsset::register($view);

		// Register widget hash JavaScript variable
		$view->registerJs("var {$this->getHashVar()} = {$this->buildClientOptions()};", View::POS_HEAD);

		// Build client script
		$js = "jQuery('{$this->getClientSelector()}').yiiSortable({$this->getHashVar()})";

		// Build client events
		if (!empty($this->clientEvents)) {
			foreach ($this->clientEvents as $clientEvent => $eventHandler) {
				if (!($eventHandler instanceof JsExpression)) {
					$eventHandler = new JsExpression($eventHandler);
				}
				$js .= ".on('{$clientEvent}', {$eventHandler})";
			}
		}

		// Register widget JavaScript
		$view->registerJs("{$js};");
	}
}
